#import <Cordova/CDVPlugin.h>

@interface SecureStorage : CDVPlugin

- (void)init:(CDVInvokedUrlCommand*)command;
- (void)get:(CDVInvokedUrlCommand*)command;
- (void)set:(CDVInvokedUrlCommand*)command;
- (void)remove:(CDVInvokedUrlCommand*)command;
- (void)keys:(CDVInvokedUrlCommand*)command;
- (void)clear:(CDVInvokedUrlCommand*)command;

- (NSString*)getValueForKey:(NSString*) key service:(NSString*)service;
- (BOOL)setValue:(NSString *)value forKey:(NSString*)key service:(NSString*) service;

@property (nonatomic, copy) id keychainAccesssibilityMapping;

@end
